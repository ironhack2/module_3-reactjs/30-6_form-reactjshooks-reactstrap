import likeImg from "../../../assets/images/like.png";

import { Row, Col } from "reactstrap";

function OutputMessage({messageOutputProp, likeImageProp}) {
    return (
        <div>
            <Row className="mt-4">
                <Col>
                    {
                        messageOutputProp.map((value, index) => {
                            return <p key={index}>{value}</p>
                        })
                    }
                </Col>
            </Row>
            {
                likeImageProp ?
                    <Row className="mt-2">
                        <Col>
                            <img src={likeImg} alt="like" width={100} />
                        </Col>
                    </Row>
                    : null
            }

        </div>
    )
}

export default OutputMessage;


