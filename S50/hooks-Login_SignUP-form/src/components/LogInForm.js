import { Row, Col, Button, Label } from "reactstrap";
import { useState } from "react";
import swal from "sweetalert";

function LogIn() {
    // LOG IN
    const [emailInput, setEmailInput] = useState('');
    const [passwordInput, setPassword] = useState('');

    const changeInputEmailHandler = (event) => {
        setEmailInput(event.target.value)
    }

    const changeInputPasswordHandler = (event) => {
        setPassword(event.target.value)
    }

    const buttonLogInHandler = () => {
        console.log("%cNút Log In được bấm", "color:blue");

        // Check Email
        const vREG = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!(vREG.test(emailInput))) {
            console.log("%cEmail không hợp lệ", "color:red");
            swal("System alert:", "Missing email address!", "error");
            return false;
        }

        //Check Password
        if (passwordInput.length < 6) {
            console.log("%cPassword không đúng!!! Password chứa ít nhất 6 ký tự", "color:red");
            swal("System alert:", "Password không đúng!!! Password chứa ít nhất 6 ký tự", "error")
            return false;
        }
        else {
            console.log("Email: ", emailInput);
            console.log("Password: ", passwordInput);
            swal("System alert:", "Login successfully!", "success");
            return true;
        }
    }


    return (
        <div>
            <h2 className="text-white mt-5"> Welcome Back!</h2>
            <Row>
                <Col sm={12} className="mt-5 form-input">
                    <input type="text" className="input-css text-white p-2" onChange={changeInputEmailHandler} placeholder=" "></input>
                    <Label className="label-placeholder">Email Address<span style={{ color: "#1ab188" }}> *</span></Label>
                </Col>
            </Row>

            <Row>
                <Col sm={12} className="mt-4 form-input">
                    <input type="password" className="input-css text-white p-2" onChange={changeInputPasswordHandler} placeholder=" "></input>
                    <Label className="label-placeholder">Password<span style={{ color: "#1ab188" }}> *</span></Label>
                </Col>
            </Row>


            <Row>
                <Col sm={12} className="mt-3">
                    <p style={{ textAlign: "right" }}>Forgot Password?</p>
                </Col>
            </Row>

            <Row className="d-flex align-items-center justify-content-center mt-2">
                <Col sm={12}>
                    <Button className="w-100 p-3" style={{ backgroundColor: "#1ab188" }} onClick={buttonLogInHandler}>LOG IN</Button>

                    {/* <button className="btn-login w-100 p-3" onClick={buttonLogInHandler}>LOG IN</button> */}
                </Col>
            </Row>
        </div>
    )
}

export default LogIn