import { Row, Col, Button, Label } from "reactstrap";
import { useState } from "react";
import swal from "sweetalert";

function SignUp() {

    // SIGN UP
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [emailSignUp, setEmailSingUp] = useState('');
    const [passwordSignUp, setPasswordSignUp] = useState('');

    const changeFirstNameHandler = (event) => {
        setFirstName(event.target.value);
    }

    const changeLastNameHandler = (event) => {
        setLastName(event.target.value);
    }

    const changeSignUpEmailHandler = (event) => {
        setEmailSingUp(event.target.value);
    }

    const changeSignUpPasswordHandler = (event) => {
        setPasswordSignUp(event.target.value);
    }


    const buttonSignUp = () => {
        console.log("%cNút Sign Up được bấm", "color: blue");

        //Check First Name
        if (firstName.length < 1) {
            console.log("%cHãy Nhập first name", "color: red");
            swal("System alert:", "Hãy Nhập First name", "error");
            return false;
        }

        //Check Last Name
        if (lastName.length < 1) {
            alert("Hãy Nhập last name");
            console.log("%cHãy Nhập last name", "color: red");
            swal("System alert:", "Hãy Nhập last name", "error");
            return false;
        }

        // Check Email
        const vREG = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!(vREG.test(emailSignUp))) {
            console.log("%cEmail không hợp lệ", "color:red");
            swal("System alert:", "Missing email address!", "error");
            return false;
        }

        //Check Password
        if (passwordSignUp.length < 6) {
            console.log("%cPassword không đúng!!! Password chứa ít nhất 6 ký tự", "color:red");
            swal("System alert:", "Password không đúng!!! Password chứa ít nhất 6 ký tự", "error")
            return false;
        }

        else {
            console.log("First name: ", firstName);
            console.log("Last name: ", lastName);
            console.log("Email: ", emailSignUp);
            console.log("Password: ", passwordSignUp);
            swal("System alert:", "Sign Up successfully!", "success");
            return true;
        }
    }


    return (
        <div>
            <h2 className="text-white mt-5"> Sign Up for Free </h2>

            <Row>
                <Col sm={6} className="mt-5 form-input">
                    <input type="text"  className="input-css text-white p-2" onChange={changeFirstNameHandler} placeholder=" "></input>
                       <Label className="label-placeholder">First Name<span style={{color:"#1ab188"}}> *</span></Label>
                </Col>

                <Col sm={6} className="mt-5 form-input">
                    <input type="text" className="input-css text-white p-2" onChange={changeLastNameHandler} placeholder=" "></input>
                    <Label className="label-placeholder">Last Name<span style={{color:"#1ab188"}}> *</span></Label>
                </Col>
            </Row>

            <Row>
                <Col sm={12} className="mt-4 form-input">
                    <input type="text" className="input-css text-white p-2" onChange={changeSignUpEmailHandler} placeholder=" "></input>
                    <Label className="label-placeholder">Email Address<span style={{color:"#1ab188"}}> *</span></Label>
                </Col>
            </Row>

            <Row>
                <Col sm={12} className="mt-4 form-input">
                    <input type="password" className="input-css text-white p-2" onChange={changeSignUpPasswordHandler} placeholder=" "></input>
                    <Label className="label-placeholder">Set A Password<span style={{color:"#1ab188"}}> *</span></Label>
                </Col>
            </Row>

            <Row className="d-flex align-items-center justify-content-center mt-5">
                <Col sm={12}>
                    <Button className="w-100 p-3" style={{ backgroundColor: "#1ab188" }} onClick={buttonSignUp}>GET STARTED</Button>

                    {/* <button className="btn-signUp w-100 p-3" onClick={buttonSignUp}>GET STARTED</button> */}
                </Col>
            </Row>
        </div>
    )
}

export default SignUp