import { useState } from "react";
import { Row, Col, Button } from "reactstrap";

import LogIn from "./LogInForm";
import SignUp from "./SignUpForm";

function HandlerForm() {

    // MENU
    const [showForm, setShowForm] = useState(true);

    const LogInMenu = () => {
        setShowForm(true);
    }

    const SignUpMenu = () => {
        setShowForm(false);
    }


    return (
        <div>
            <Row className="d-flex align-items-center mt-5">
                <Col sm={4} className="text-center mx-auto bg-content p-5">
                    <Row className="d-flex align-items-center justify-content-center mt-4">
                        <Col sm={12}>
                            <Button className="w-50 p-2"  style={{backgroundColor: showForm ? "#435359" : "#1ab188"}}  onClick={SignUpMenu}>Sing Up</Button>
                            <Button className="w-50 p-2" style= {{backgroundColor: showForm ? "#1ab188" : "#435359"}} onClick={LogInMenu}>Log In</Button>

                            {/* <button className= {showForm ? "btn-color2" : "btn-color1"}onClick={LogInMenu}>Sing Up</button> */}
                            {/* <button className={showForm ? "btn-color1" : "btn-color2"} onClick={SignUpMenu}>Log In</button> */}

                        </Col>
                    </Row>

                   { showForm  ?  <LogIn /> : <SignUp  />   }

                </Col>
            </Row>
        </div>
    )
}

export default HandlerForm