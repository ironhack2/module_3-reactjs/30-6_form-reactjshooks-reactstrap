import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import HandlerForm from "./components/HandlerForm";

function App() {
  return (
    <div>
      <HandlerForm />
    </div>
  );
}

export default App;
